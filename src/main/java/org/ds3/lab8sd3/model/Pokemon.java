package org.ds3.lab8sd3.model;

import jakarta.persistence.*;
import lombok.*;
import org.ds3.lab8sd3.shared.PokemonDTO;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Document("pokemons")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Data
public class Pokemon {

    @MongoId
    private String id;
    private String name;
    private String description;
    private int level;

    public Pokemon(PokemonDTO pokemonDTO) {
        this.name = pokemonDTO.name();
        this.description = pokemonDTO.description();
        this.level = pokemonDTO.level();
    }
}
