package org.ds3.lab8sd3.repository;

import org.ds3.lab8sd3.model.Pokemon;
//import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PokemonRepository extends MongoRepository<Pokemon, String> {
}
