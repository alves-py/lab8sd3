package org.ds3.lab8sd3.controller;

import org.ds3.lab8sd3.model.Pokemon;
import org.ds3.lab8sd3.services.PokemonServices;
import org.ds3.lab8sd3.shared.PokemonDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping
public class PokemonController {
    @Autowired
    private PokemonServices pokemonServices;

    @GetMapping
    public ResponseEntity<List<Pokemon>> getAllPokemons() {
        List<Pokemon> pokemons = pokemonServices.getPokemons();
        return new ResponseEntity<>(pokemons, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Pokemon> createPokemon(@RequestBody PokemonDTO pokemonDTO) {
        Pokemon pokemon = new Pokemon(pokemonDTO);
        pokemonServices.addPokemon(pokemon);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
