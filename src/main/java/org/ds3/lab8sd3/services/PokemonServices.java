package org.ds3.lab8sd3.services;

import org.ds3.lab8sd3.model.Pokemon;
import org.ds3.lab8sd3.repository.PokemonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PokemonServices {
    @Autowired
    private PokemonRepository pokemonRepository;

    public List<Pokemon> getPokemons() {
        return pokemonRepository.findAll();
    }
    public void addPokemon(Pokemon pokemon) {
        pokemonRepository.save(pokemon);
    }
}
