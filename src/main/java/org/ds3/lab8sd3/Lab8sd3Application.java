package org.ds3.lab8sd3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab8sd3Application {

    public static void main(String[] args) {
        SpringApplication.run(Lab8sd3Application.class, args);
    }

}
