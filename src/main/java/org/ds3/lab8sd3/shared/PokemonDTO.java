package org.ds3.lab8sd3.shared;

public record PokemonDTO(String name, String description, int level) {
}
